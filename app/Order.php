<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $table = 'orders';
    protected $primaryKey = 'ordernumber';

    public function user(){
      return $this->belongsTo('App\User');
    }
    public function orderdetails(){
      return $this->hasMany('App\OrderDetail');
    }
}
