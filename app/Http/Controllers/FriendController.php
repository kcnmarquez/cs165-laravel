<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use Validator;
use Auth;
use Redirect;
use Illuminate\Support\Facades\Input;
use Hash;

class FriendController extends Controller
{
  public function login()
  {
      return \View::make('friend.login');
  }

  public function doLogin()
  {
    $rules = array(
        'email'    => 'required|email',
        'password' => 'required|alphaNum|min:3' // password can only be alphanumeric and has to be greater than 3 characters
    );

    $validator = Validator::make(Input::all(), $rules);

    // if the validator fails, redirect back to the form
    if ($validator->fails()) {
        return Redirect::to('/login')
            ->withErrors($validator) // send back all errors to the login form
            ->withInput(Input::except('password')); // send back the input (not the password) so that we can repopulate the form
    } else {

        $userdata = array(
            'email'     => Input::get('email'),
            'password'  => Input::get('password')
        );

        if (Auth::attempt($userdata)) {
            return Redirect::to('/home');
        } else {
            // dd(Auth::attempt($userdata));//dd($userdata);
            return Redirect::to('/login')
              ->withErrors(['incorrect'=>'Email or Password incorrect!'])
              // ->withErrors([$this->loginUsername() => $this->getFailedLoginMessage(),])
              ->withInput(Input::except('password'));
        }
    }
  }

  public function logout()
  {
      Auth::logout(); // log the user out of our application
      return Redirect::to('/'); // redirect the user to the login screen
  }
}
