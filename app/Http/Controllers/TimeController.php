<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use Validator;
use Auth;
use Redirect;
use Carbon\Carbon;
use Illuminate\Support\Facades\Input;
use Gloudemans\Shoppingcart\Facades\Cart;
use App\User,App\Time;

class TimeController extends Controller
{
  public function __construct()
  {
    $this->middleware('auth');
  }

  public function buy($id)
  {
    $time = \DB::table('times')
              ->join('users', 'users.id', '=', 'times.user_id')
              ->where('time_id',$id)
              ->first();
    $rows  = Cart::content();
    if($rows->where('id', $id)->first())  //if item is already in cart
    {
      $rowId = $rows->where('id', $id)->first()->rowId;
      $item = Cart::get($rowId);
      if($item->qty+Input::get('quantity')/$time->mintime <= $time->available) #cart.quantity does not exceed $time->available*$time->mintime
      {
        Cart::add($id,$time->firstname.' '.$time->lastname,Input::get('quantity')/$time->mintime,$time->mintime);
      } else {
        dd('Did not update!',$item, Input::get('quantity'),$item->qty+Input::get('quantity')/$time->mintime,$time->available);
      }
    } else {
      Cart::add($id,$time->firstname.' '.$time->lastname,Input::get('quantity')/$time->mintime,$time->mintime); #id,productname,quantity,price
    }
    flash('Successfully added item to cart!', 'success');
    return Redirect::to('/times');
  }

  public function removeCart($rowId)
  {
    flash('Successfully removed item from cart!', 'success');
    Cart::remove($rowId);
    return Redirect::to('/home');
  }

  public function index()
  {
    // $times = \DB::table('times')
    //           ->join('users', 'users.id', '=', 'times.user_id')
    //           ->where('users.id','<>',Auth::user()->id)
    //           ->orderBy('time_id','asc')
    //           ->get();
    $times = Time::with('user')
                ->where('available','>',0)
                ->whereDate('date','>',Carbon::now()->toDateString())
                ->get();
    return \View::make('time.index')->with('times', $times);
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return Response
   */
  public function create()
  {
    $userids = \DB::table('users')
              ->where('role','=','1')
              ->orderBy('id','asc')
              ->pluck('id');

    return \View::make('time.create')->with('userids', $userids);
  }

  /**
   * Store a newly created resource in storage.
   *
   * @return Response
   */
  public function store()
  {
      $rules = array(
          'user_id'    => 'required',
          'date'       => 'required|date',
          'starttime'  => 'required|date_format:H:i',
          'endtime'    => 'required|date_format:H:i|after:starttime',
          'mintime'    => 'required|integer',
      );
      $validator = Validator::make(Input::all(), $rules);

      // process the login
      if ($validator->fails()) {
          return Redirect::to('times/create')
              ->withErrors($validator)
              ->withInput();
      } else {
          // store
          $date = Carbon::createFromFormat('Y-m-d', Input::get('date'));
          $starttime = Carbon::createFromFormat('Y-m-d H:i', $date->toDateString().' '.Input::get('starttime'));
          $endtime = Carbon::createFromFormat('Y-m-d H:i', $date->toDateString().' '.Input::get('endtime'));
          // \DB::table('times')->insert(array(
          //     'user_id'   => Input::get('user_id'),
          //     'date'      => $date->toDateString(),
          //     'starttime' => $starttime->toTimeString(),
          //     'endtime'   => $endtime->toTimeString(),
          //     'mintime'   => Input::get('mintime'),
          //     'available' => ($endtime->diffInMinutes($starttime))/Input::get('mintime'),
          //     'description'=> Input::get('description'),
          // ));
          $time = new Time;

          $user = User::find(Input::get('user_id'));
          $time->user()->associate($user);
          $time->date = $date->toDateString();
          $time->starttime = $starttime->toTimeString();
          $time->endtime = $endtime->toTimeString();
          $time->mintime = Input::get('mintime');
          $time->available = ($endtime->diffInMinutes($starttime))/Input::get('mintime');
          $time->description = Input::get('description');
          $time->save();
          // redirect
          flash('Successfully created time!', 'success');
          return Redirect::to('/times');
      }
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return Response
   */
  public function show($id)
  {
    $time = \DB::table('times')->where('time_id', $id)->first();
    $owner = \DB::table('users')->where('id', $time->user_id)->first();
    return \View::make('time.show')
      ->with('time', $time)
      ->with('owner', $owner);
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return Response
   */
  public function edit($id)
  {
    if(Auth::user()->role == 2)
    {
      $time = \DB::table('times')->where('time_id',$id)->first();
      $userids = \DB::table('users')
                ->where('role','=','1')
                ->orderBy('id','asc')
                ->pluck('id');

      return \View::make('time.edit')
          ->with('time', $time)
          ->with('userids', $userids)
          ->with('input',Input::all());
    } else {
      flash('Unauthorized access!', 'danger');
      return Redirect::to('/home');
    }
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  int  $id
   * @return Response
   */
  public function update($id)
  {
      $rules = array(
          'user_id'   => 'required',
          'date'      => 'required|date',
          'starttime' => 'required|date_format:H:i',
          'endtime'   => 'required|date_format:H:i|after:starttime',
          'mintime'   => 'required|integer'
      );
      $validator = Validator::make(Input::all(), $rules);

      // process the login
      if ($validator->fails()) {
          return Redirect::to('times/'.$id.'/edit')
              ->withErrors($validator)
              ->withInput();
      } else {
          // store
          // \DB::table('times')
          //   ->where('time_id', $id)
          //   ->update(array(
          //       'user_id'   => Input::get('user_id'),
          //       'date'      => Input::get('date'),
          //       'starttime' => Input::get('starttime'),
          //       'endtime'   => Input::get('endtime'),
          //   ));
          $time = Time::where('time_id', $id)->first();

          $user = User::find(Input::get('user_id'));
          $time->user()->associate($user);
          $date = Carbon::createFromFormat('Y-m-d', Input::get('date'));
          $starttime = Carbon::createFromFormat('Y-m-d H:i', $date->toDateString().' '.Input::get('starttime'));
          $endtime = Carbon::createFromFormat('Y-m-d H:i', $date->toDateString().' '.Input::get('endtime'));
          $time->date = $date->toDateString();
          $time->starttime = $starttime->toTimeString();
          $time->endtime = $endtime->toTimeString();
          $time->mintime = Input::get('mintime');
          $time->available = ($endtime->diffInMinutes($starttime))/Input::get('mintime');
          $time->description = Input::get('description');
          $time->save();
          // redirect
          flash('Successfully updated time!', 'success');
          return Redirect::to('times/'.$id);
      }
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return Response
   */
  public function destroy($id)
  {
      //
  }
}
