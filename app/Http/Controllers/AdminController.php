<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use Validator;
use Auth;
use Redirect;
use Illuminate\Support\Facades\Input;
use Hash;

class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $admins = \DB::table('admins')->get();
        // $admins = \App\Admin::all(); //doesn't work :<

        // return view('admin.index', ['admins' => $admins]);
        return \View::make('admin.index')->with('admins', $admins);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function register()
    {
        return \View::make('admin.register');
    }

    public function doRegister()
    {
        // validate
        // read more on validation at http://laravel.com/docs/validation
        $rules = array(
            'username'   => 'required|alphaNum|unique:admins,username',
            'firstname'  => 'required|alphaNum',
            'lastname'   => 'required|alphaNum',
            'email'      => 'required|email',
            'password'   => 'required|alphaNum|min:3',
            'password_confirmation'   => 'required|same:password'
        );
        $validator = Validator::make(Input::all(), $rules);

        // process the login
        if ($validator->fails()) {
            return Redirect::to('admins/register')
                ->withErrors($validator)
                ->withInput(Input::except('password'));
        } else {
            // store
            \DB::table('admins')->insert(array(
                'username' => Input::get('username'),
                'firstname'=> Input::get('firstname'),
                'lastname' => Input::get('lastname'),
                'email'    => Input::get('email'),
                'password' => Hash::make(Input::get('password')),
            ));
            // redirect
            // Session::flash('message', 'Successfully created nerd!');
            return Redirect::to('/');
        }
    }

    public function login()
    {
        return \View::make('admin.login');
    }

    public function doLogin()
    {
      $rules = array(
          'email'    => 'required|email',
          'password' => 'required|alphaNum|min:3' // password can only be alphanumeric and has to be greater than 3 characters
      );

      $validator = Validator::make(Input::all(), $rules);

      // if the validator fails, redirect back to the form
      if ($validator->fails()) {
          return Redirect::to('admins/login')
              ->withErrors($validator) // send back all errors to the login form
              ->withInput(Input::except('password')); // send back the input (not the password) so that we can repopulate the form
      } else {

          $userdata = array(
              'email'     => Input::get('email'),
              'password'  => Input::get('password')
          );

          if (Auth::attempt($userdata)) {
              return Redirect::to('admins');
          } else {
              // dd(Auth::attempt($userdata));//dd($userdata);
              return Redirect::to('admins/login')
                ->withErrors(['incorrect'=>'Email or Password incorrect!'])
              //   // ->withErrors([$this->loginUsername() => $this->getFailedLoginMessage(),])
                ->withInput(Input::except('password'));
          }
      }
    }

    public function logout()
    {
        Auth::logout(); // log the user out of our application
        return Redirect::to('/'); // redirect the user to the login screen
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
