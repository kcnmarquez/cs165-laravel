<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use Validator;
use Auth;
use Redirect;
use Illuminate\Support\Facades\Input;
use Hash;
use App\User,App\Order;

class UserController extends Controller
{
  public function index()
  {
    if(!Auth::guest() and Auth::user()->role == 2)
    {
      $users = \DB::table('users')
      ->orderBy('id','asc')
      ->get();
      return \View::make('user.index')->with('users', $users);
    }
    else
    {
      flash('Unauthorized access!', 'danger');
      return Redirect::to('/home');
    }
  }

  public function show($id)
  {
    if(!Auth::guest() && (Auth::user()->id == $id || Auth::user()->role == 2))
    {
      $user = User::find($id);
      $orders = Order::where('user_id','=',$id)
                ->orderBy('updated_at','desc')
                ->get();
      return \View::make('user.show')
        ->with('user', $user)
        ->with('orders', $orders);
    }
    else
    {
      flash('Unauthorized access!', 'danger');
      return Redirect::to('/home');
    }
  }

  public function edit($id)
  {
    if(!Auth::guest() && Auth::user()->id == $id)
    {
      $user = \DB::table('users')->where('id', $id)->first();
      return \View::make('user.edit')
        ->with('user', $user)
        ->with('input',Input::all());
    }
    else
    {
      flash('Unauthorized access!', 'danger');
      return Redirect::to('/home');
    }
  }

  public function update($id)
  {
    $rules = array(
        'username'  => 'required|unique:users,username,'.$id,
        'password'  => 'required_unless:password,""|alphaNum|min:3',
        'firstname' => 'required',
        'lastname'  => 'required',
        'email'     => 'required|email',
        'mobilenum' => 'required',
    );
    $validator = Validator::make(Input::all(), $rules);

    // process the login
    if ($validator->fails()) {
        return Redirect::to('users/'.$id.'/edit')
            ->withErrors($validator)
            ->withInput(Input::except('password'));
    } else {
        // store

        if(Input::get('password') != '')
        {
          \DB::table('users')
            ->where('id', $id)
            ->update(array(
                'username'  => Input::get('username'),
                'password'  => Hash::make(Input::get('password')),
                'firstname' => Input::get('firstname'),
                'lastname'  => Input::get('lastname'),
                'email'     => Input::get('email'),
                'mobilenum' => Input::get('mobilenum'),
            ));
        }
        else
        {
          \DB::table('users')
            ->where('id', $id)
            ->update(array(
                'username'  => Input::get('username'),
                'firstname' => Input::get('firstname'),
                'lastname'  => Input::get('lastname'),
                'email'     => Input::get('email'),
                'mobilenum' => Input::get('mobilenum'),
            ));
        }

        flash('Successfully updated profile!', 'success');
        return Redirect::to('users/'.$id);
    }
  }

  public function login()
  {
      return \View::make('user.login');
  }

  public function doLogin()
  {
    $rules = array(
        'email'    => 'required|email',
        'password' => 'required|alphaNum|min:3' // password can only be alphanumeric and has to be greater than 3 characters
    );

    $validator = Validator::make(Input::all(), $rules);

    // if the validator fails, redirect back to the form
    if ($validator->fails()) {
        return Redirect::to('/login')
            ->withErrors($validator) // send back all errors to the login form
            ->withInput(Input::except('password')); // send back the input (not the password) so that we can repopulate the form
    } else {

        $userdata = array(
            'email'     => Input::get('email'),
            'password'  => Input::get('password')
        );

        if (Auth::attempt($userdata)) {
            flash('Successfully logged in!', 'success');
            return Redirect::to('/home');
        } else {
            // dd(Auth::attempt($userdata));//dd($userdata);
            flash('Incorrect Email or Password!', 'danger');
            return Redirect::to('/login')
              ->withInput(Input::except('password'));
        }
    }
  }

  public function logout()
  {
      Auth::logout(); // log the user out of our application
      flash('Successfully logged out!', 'success');
      return Redirect::to('/'); // redirect the user to the login screen
  }
}
