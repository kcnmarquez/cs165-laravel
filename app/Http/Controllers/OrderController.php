<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\User, App\Time, App\Order, App\OrderDetail;
use Redirect;
use Auth;
use Gloudemans\Shoppingcart\Facades\Cart;

class OrderController extends Controller
{
  public function index()
  {
    $user = User::find(Auth::user()->id);
    $order = $user->orders()->create([]);
    $order->save();

    foreach(Cart::content() as $row)
    {
      $time = Time::find($row->id);
      $odetail = new OrderDetail;
      $odetail->order()->associate($order);
      $odetail->time()->associate($time);
      $odetail->quantity = $row->qty;
      $odetail->save();

      $odetail->time->available = $odetail->time->available - $odetail->quantity;
      $odetail->time->save();
    }

    Cart::destroy();

    flash('Successfully checked out!', 'success');
    return Redirect::to('/home');
  }

  public function show($id)
  {
    $order = Order::find($id);
    if(!Auth::guest() && (Auth::user()->id == $order->user->id || Auth::user()->role == 2))
    {
      return \View::make('order.show')
        ->with('order', $order);
    }
    else
    {
      flash('Unauthorized access!', 'danger');
      return Redirect::to('/home');
    }
  }
}
