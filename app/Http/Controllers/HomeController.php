<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use Auth;
use App\Order;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Auth::user()->role == 2)
        {
          $orders = Order::orderBy('updated_at','desc')
                    ->get();
          return \View::make('home')
            ->with('orders', $orders);
        }
        else
        {
          return view('home');
        }
    }
}
