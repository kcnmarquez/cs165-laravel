<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

// Route::auth();

Route::get('/login', 'UserController@login');
Route::post('/login', 'UserController@doLogin');
Route::get('/logout', 'UserController@logout');
Route::resource('users', 'UserController',
                ['only' => ['index','show','edit','update']]);

Route::post('times/{id}/buy', 'TimeController@buy');
Route::get('times/{rowId}/remove', 'TimeController@removeCart');
Route::resource('times', 'TimeController');

Route::resource('orders', 'OrderController',
                ['only' => ['index','show']]);

Route::get('/home', 'HomeController@index');
