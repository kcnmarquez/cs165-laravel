<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderDetail extends Model
{
  protected $table = 'orderdetails';
  protected $fillable = ['status'];

  public function time(){
    return $this->belongsTo('App\Time');
  }
  public function order(){
    return $this->belongsTo('App\Order');
  }
}
