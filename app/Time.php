<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Time extends Model
{
    protected $table = 'times';
    protected $primaryKey = 'time_id';
    public $timestamps = false;
    protected $fillable = ['date','starttime','endtime','mintime','available','description'];

    public function user(){
      return $this->belongsTo('App\User');
    }
    public function orderdetails(){
      return $this->hasMany('App\OrderDetail');
    }
}
