<?php

use Illuminate\Database\Seeder;

class TimeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('times')->delete();
        \DB::table('times')->insert(array(
            'user_id'   => 1,
            'date'      => '2016-12-20',
            'starttime' => '1:30',
            'endtime'   => '2:00',
            'mintime'   => 30,
            'available' => 1,
        ));
        \DB::table('times')->insert(array(
            'user_id'   => 1,
            'date'      => '2016-12-20',
            'starttime' => '2:00',
            'endtime'   => '2:30',
            'mintime'   => 10,
            'available' => 3,
        ));
        \DB::table('times')->insert(array(
            'user_id'   => 2,
            'date'      => '2016-12-20',
            'starttime' => '1:30',
            'endtime'   => '2:30',
            'mintime'   => 10,
            'available' => 6,
        ));
        \DB::table('times')->insert(array(
            'user_id'   => 3,
            'date'      => '2016-12-25',
            'starttime' => '11:30',
            'endtime'   => '2:30',
            'mintime'   => 30,
            'available' => 6,
        ));
        \DB::table('times')->insert(array(
            'user_id'   => 3,
            'date'      => '2016-12-25',
            'starttime' => '3:00',
            'endtime'   => '3:30',
            'mintime'   => 30,
            'available' => 1,
        ));
    }
}
