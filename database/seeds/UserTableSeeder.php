<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      \DB::table('users')->delete();
      \DB::table('users')->insert(array(
          'username' => 'user1',
          'firstname'=> 'User',
          'lastname' => 'One',
          'email'    => 'user1@sample.com',
          'mobilenum'=> '09123456789',
          'password' => Hash::make('password'),
          'role' => 1,
      ));
      \DB::table('users')->insert(array(
          'username' => 'user2',
          'firstname'=> 'User',
          'lastname' => 'Two',
          'email'    => 'user2@sample.com',
          'mobilenum'=> '09123456780',
          'password' => Hash::make('password'),
          'role' => 1,
      ));
      \DB::table('users')->insert(array(
          'username' => 'user3',
          'firstname'=> 'User',
          'lastname' => 'Three',
          'email'    => 'user3@sample.com',
          'mobilenum'=> '09123456781',
          'password' => Hash::make('password'),
          'role' => 1,
      ));
      \DB::table('users')->insert(array(
          'username' => 'adminone',
          'firstname'=> 'Admin',
          'lastname' => 'One',
          'email'    => 'admin1@sample.com',
          'password' => Hash::make('password'),
          'role' => 2,
      ));
    }
}
