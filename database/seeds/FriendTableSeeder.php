<?php

use Illuminate\Database\Seeder;

class FriendTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      \DB::table('friends')->delete();
      \DB::table('friends')->insert(array(
          'username' => 'friend1',
          'firstname'=> 'Friend',
          'lastname' => 'One',
          'email'    => 'friend1@sample.com',
          'mobilenum'=> '09123456789',
          'password' => Hash::make('password'),
      ));
      \DB::table('friends')->insert(array(
          'username' => 'friend2',
          'firstname'=> 'Friend',
          'lastname' => 'Two',
          'email'    => 'friend2@sample.com',
          'mobilenum'=> '09123456780',
          'password' => Hash::make('password'),
      ));
      \DB::table('friends')->insert(array(
          'username' => 'friend3',
          'firstname'=> 'Friend',
          'lastname' => 'Three',
          'email'    => 'friend3@sample.com',
          'mobilenum'=> '09123456781',
          'password' => Hash::make('password'),
      ));
    }
}
