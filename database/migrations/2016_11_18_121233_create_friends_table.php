<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFriendsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('friends', function($table)
      {
          $table->increments('id');
          $table->string('username')->unique();
          $table->string('password');
          $table->string('firstname');
          $table->string('lastname');
          $table->string('email')->unique();
          $table->string('mobilenum');
          $table->rememberToken();
          $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::drop('friends');
    }
}
