<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderdetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('orderdetails', function (Blueprint $table) {
          $table->increments('id');
          $table->integer('order_id')->unsigned();
          $table->integer('time_id')->unsigned();
          $table->integer('quantity');
          $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::drop('orderdetails');
    }
}
