<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTimeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('times', function($table)
      {
          $table->increments('time_id');
          $table->integer('user_id')->unsigned();
          $table->date('date');
          $table->time('starttime');
          $table->time('endtime');
          $table->integer('mintime'); //minimum time a buyer can order (minutes)
          $table->integer('available'); //quantity of available times left
          $table->text('description')->nullable();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::drop('times');
    }
}
