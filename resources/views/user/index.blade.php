@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">
                  Users
                </div>
                <div class="panel-body">
                  <table class="table">
                    <thead>
                      <th>User ID</th>
                      <th>Username</th>
                      <th>Name</th>
                      <th>Role</th>
                      <th></th>
                    </thead>
                    <tbody>
                    @foreach ($users as $user)
                      <tr>
                        <td>{{ $user->id }}</td>
                        <td>{{ $user->username }}</td>
                        <td>{{ $user->firstname }} {{ $user->lastname }}</td>
                        @if($user->role == 1)
                        <td>User</td>
                        @else
                        <td>Admin</td>
                        @endif
                        <td>
                          <a class="btn btn-default" href="{{ URL::to('users/'.$user->id) }}">See Profile</a>
                        </td>
                      </tr>
                    @endforeach
                    </tbody>
                  </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
