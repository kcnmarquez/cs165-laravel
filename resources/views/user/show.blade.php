@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading clearfix">
                  <h4 class="panel-title pull-left" style="padding-top: 7.5px;">
                    {{ $user->firstname }} {{ $user->lastname }}
                  </h4>
                  @if (Auth::user()->role == 1)
                  <a class="btn btn-default btn-sm pull-right" href="{{ URL::to('users/'.$user->id.'/edit') }}">Edit</a>
                  @endif
                </div>
                <div class="panel-body">
                  <table class='table'>
                    <tr>
                      <th>Username:</th>
                      <td>{{ $user->username }}</td>
                    </tr>
                    <tr>
                      <th>First Name:</th>
                      <td>{{ $user->firstname }}</td>
                    </tr>
                    <tr>
                      <th>Last Name:</th>
                      <td>{{ $user->lastname }}</td>
                    </tr>
                    <tr>
                      <th>Email:</th>
                      <td>{{ $user->email }}</td>
                    </tr>
                    @if ($user->role == 1)
                    <tr>
                      <th>Mobile number:</th>
                      <td>{{ $user->mobilenum }}</td>
                    </tr>
                    @endif
                  </table>
                </div>
            </div>
        </div>
    </div>
    @if($user->role == 1)
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading clearfix">
                  <h4 class="panel-title pull-left" style="padding-top: 7.5px;">
                    Order History
                  </h4>
                </div>
                <div class="panel-body">
                  <table class='table'>
                    <thead>
                      <tr>
                        <th>Order number</th>
                        <th>Date of checkout</th>
                        <th></th>
                      </tr>
                    </thead>
                    <tbody>
                    @if(count($orders))
                      @foreach($orders as $order)
                      <tr>
                        <td>{{ $order->ordernumber }}</td>
                        <td>{{ $order->updated_at }}</td>
                        <td><a class="btn btn-link" href="{{ URL::to('/orders/'.$order->ordernumber) }}">See details</a></td>
                      </tr>
                      @endforeach
                    @else
                      <tr><td>No orders yet.</td></tr>
                    @endif
                    </tbody>
                  </table>
                </div>
            </div>
        </div>
    </div>
    @endif
</div>
@endsection
