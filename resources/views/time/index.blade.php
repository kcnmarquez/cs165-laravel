@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading clearfix">
                  <h4 class="panel-title pull-left" style="padding-top: 7.5px;">
                    Times
                  </h4>
                  @if (Auth::user()->role == 2)
                  <a class="btn btn-default btn-sm pull-right" href="{{ URL::to('times/create') }}">Add Time</a>
                  @endif
                </div>
                <div class="panel-body">
                  <table class="table">
                    <thead>
                      <th>Available Users</th>
                      <th>Date</th>
                      <th>Time</th>
                      <th>Available Minutes</th>
                      <th>Description</th>
                      <th></th>
                    </thead>
                    <tbody>
                    @if(count($times))
                      @foreach ($times as $time)
                        <tr>
                          <td>{{ $time->user->firstname }} {{ $time->user->lastname }}</td>
                          <td>{{ date('F d, Y', strtotime($time->date)) }}</td>
                          <td>{{ date('H:i', strtotime($time->starttime)) }} - {{ date('H:i', strtotime($time->endtime)) }}</td>
                          <td>{{ $time->mintime*$time->available }}</td>
                          <td>{{ $time->description }}</td>
                          @if(Auth::user()->role == 1)
                          <td>
                            <form class="form-inline" role="form" method="POST" action="{{ url('/times/'.$time->time_id.'/buy') }}">
                              {{ csrf_field() }}
                              <div class="form-group{{ $errors->has('incorrect') ? ' has-error' : '' }}">
                                <input id="quantity" type="number" name="quantity" style="width:60px;"
                                  value='{{ $time->mintime }}'
                                  step="{{ $time->mintime }}"
                                  max="{{ $time->mintime*$time->available }}"
                                  min='{{ $time->mintime }}'
                                />
                              </div>
                              <button type="submit" class="btn btn-primary">
                                <i class="fa fa-btn fa-cart-plus"></i>
                              </button>
                            </form>
                            <!-- <a class="btn btn-default" href="{{ URL::to('times/'.$time->time_id.'/buy') }}">Buy</a> -->
                          </td>
                          @else
                          <td>
                            <a class="btn btn-default" href="{{ URL::to('times/'.$time->time_id.'/edit') }}">Edit</a>
                          </td>
                          @endif
                        </tr>
                      @endforeach
                    @else
                      <tr><td colspan=5>No available times.</td></tr>
                    @endif
                    </tbody>
                  </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
