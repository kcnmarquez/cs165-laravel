@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading clearfix">
                  <h4 class="panel-title pull-left" style="padding-top: 7.5px;">
                    {{ $owner->firstname }} {{ $owner->lastname }}'s Time
                  </h4>
                  @if (Auth::user()->role == 2)
                  <a class="btn btn-default btn-sm pull-right" href="{{ URL::to('times/'.$time->time_id.'/edit') }}">Edit</a>
                  @endif
                </div>
                <div class="panel-body">
                  <table class='table'>
                    <tr>
                      <th>Date:</th>
                      <td>{{ date('F d, Y', strtotime($time->date)) }}</td>
                    </tr>
                    <tr>
                      <th>Time:</th>
                      <td>{{ date('H:i', strtotime($time->starttime)) }} - {{ date('H:i', strtotime($time->endtime)) }}</td>
                    </tr>
                    <tr>
                      <th>Minimum Time:</th>
                      <td>{{ $time->mintime }}</td>
                    </tr>
                    <tr>
                      <th>Description:</th>
                      <td>{{ $time->description }}</td>
                    </tr>
                  </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
