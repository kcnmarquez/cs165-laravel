@extends('layouts.app')

@section('content')
<div class="jumbotron">
  <div class="container">
    <h1>Welcome to TimeShop!</h1>
    <p>Think you can't buy time? Think again.</p>
  </div>
</div>
@endsection
