@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>

                <div class="panel-body">
                  @if(Auth::user()->role == 1)
                    <table class='table'>
                      <thead>
                        <tr>
                          <th>Time with</th>
                          <th>Qty</th>
                          <th>Minutes</th>
                          <th>Subtotal</th>
                          <th></th>
                        </tr>
                      </thead>
                      <tbody>
                        @foreach(Cart::content() as $row)
                        <tr>
                           <td>
                               <p><strong>{{$row->name}}</strong></p>
                           </td>
                           <td>{{$row->qty}}</td>
                           <td>{{$row->price}} minutes</td>
                           <td>{{$row->subtotal}} minutes</td>
                           <td>
                             <a class="btn btn-default" href="{{ URL::to('times/'.$row->rowId.'/remove') }}">
                               <i class="fa fa-btn fa-trash"></i>
                             </a>
                           </td>
                        </tr>
                        @endforeach
                      </tbody>
                      <tfoot>
                        <tr>
                            <td colspan="2">&nbsp;</td>
                            <td>Total</td>
                            <td>{{ Cart::total() }}</td>
                            <td>
                              <a class="btn btn-default" href="{{ URL::to('/orders') }}">
                                <i class="fa fa-btn fa-shopping-cart"></i> Checkout
                              </a>
                            </td
                        </tr>
                      </tfoot>
                    </table>
                  @else
                    <table class="table">
                      <thead>
                        <th>Ordernumber</th>
                        <th>User Name</th>
                        <th></th>
                      </thead>
                      <tbody>
                        @foreach($orders as $order)
                        <tr>
                          <td>{{ $order->ordernumber }}</td>
                          <td>{{ $order->user->username }}</td>
                          <td><a class="btn btn-link" href="{{ URL::to('/orders/'.$order->ordernumber) }}">See details</a></td>
                        </tr>
                        @endforeach
                      </tbody>
                    </table>
                  @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
