<!DOCTYPE html>
<html>
<head>
    <title>Look! I'm CRUDding</title>
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css">
</head>
<body>
<div class="container">

<nav class="navbar navbar-inverse">
    <div class="navbar-header">
        <a class="navbar-brand" href="{{ URL::to('admins') }}">Nerd Alert</a>
    </div>
    <ul class="nav navbar-nav">
        <li><a href="{{ URL::to('admins') }}">View All Nerds</a></li>
        <!-- <li><a href="{{ URL::to('admins/create') }}">Create a Nerd</a> -->
        @if (Auth::check())
        <li><a href="{{ URL::to('admins/logout') }}">Logout</a></li>
        @endif
    </ul>
</nav>

<h1>All the Nerds</h1>

<!-- will be used to show any messages
@if (Session::has('message'))
    <div class="alert alert-info">{{ Session::get('message') }}</div>
@endif-->

<table class="table table-striped table-bordered">
    <thead>
        <tr>
            <td>ID</td>
            <td>username</td>
            <td>firstname</td>
            <td>lastname</td>
            <td>email</td>
        </tr>
    </thead>
    <tbody>
    @foreach($admins as $key => $value)
        <tr>
            <td>{{ $value->id }}</td>
            <td>{{ $value->username }}</td>
            <td>{{ $value->firstname }}</td>
            <td>{{ $value->lastname }}</td>
            <td>{{ $value->email }}</td>

            <!--  -->
        </tr>
    @endforeach
    </tbody>
</table>

</div>
</body>
</html>
