@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading clearfix">
                  <h4 class="panel-title pull-left" style="padding-top: 7.5px;">
                    Order Number {{$order->ordernumber}}
                  </h4>
                </div>
                <div class="panel-body">
                  <h4><strong>User:</strong> {{$order->user->firstname}} {{$order->user->lastname}}</h4>
                  <table class='table'>
                    <thead>
                      <tr>
                        <th>Time With</th>
                        <th>Date</th>
                        <th>Scheduled Time</th>
                        <th>Minutes bought</th>
                        <th></th>
                      </tr>
                    </thead>
                    <tbody>
                      @foreach($order->orderdetails as $odetail)
                      <tr>
                        <td>{{ $odetail->time->user->firstname }} {{ $odetail->time->user->lastname }}</td>
                        <td>{{ date('F d, Y', strtotime($odetail->time->date)) }}</td>
                        <td>{{ date('H:i', strtotime($odetail->time->starttime)) }} - {{ date('H:i', strtotime($odetail->time->endtime)) }}</td>
                        <td>{{ $odetail->time->mintime*$odetail->quantity }}</td>
                      </tr>
                      @endforeach
                    </tbody>
                  </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
